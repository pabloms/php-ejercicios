<?php

$file = $_GET['text'];

if (str_contains($file, '.exe')) {
    echo strtoupper($file);
} elseif (str_contains($file, '.db')) {
    echo strtolower($file);
} else {
    echo $file;
}
