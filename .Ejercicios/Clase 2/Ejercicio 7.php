<?php

$paises = [
    "Italy" => "Rome",
    "Luxembourg" => "Luxembourg",
    "Belgium" => "Brussels",
    "Denmark" => "Copenhagen",
    "Finland" => "Helsinki",
    "France" => "Paris",
    "Slovakia" => "Bratislava",
    "Slovenia" => "Ljubljana",
    "Germany" => "Berlin",
    "Greece" => "Athens",
    "Ireland" => "Dublin",
    "Netherlands" => "Amsterdam",
    "Portugal" => "Lisbon",
    "Spain" => "Madrid",
    "Sweden" => "Stockholm",
    "United Kingdom" => "London",
    "Cyprus" => "Nicosia",
    "Lithuania" => "Vilnius",
    "Czech Republic" => "Prague",
    "Estonia" => "Tallin",
    "Hungary" => "Budapest",
    "Latvia" => "Riga", "Malta" => "Valetta",
    "Austria" => "Vienna",
    "Poland" => "Warsaw"
];

// Ordena por valor pero las claves pasan a ser numericas 0 => Athenas
// sort($paises);

// asort ordena por el valor del array
asort($paises);

// ksort ordena por las claves del array
// ksort($paises);

foreach ($paises as $pais => $capital) {
    echo "La capital de $pais es $capital <br>";
};
