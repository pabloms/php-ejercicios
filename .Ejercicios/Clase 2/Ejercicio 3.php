<?php

$number = $_GET['number'];
$result = $number;
$count = 0;

// Solucion 1
// do {
//     $count++;
//     $result = ($number)**$count;
// } while ($result <= 10000);


// Solucion 2
while ($result <= 10000) {

    $result = pow($number, $count);

    if ($result >= 10000) {
        break;
    }

    $count++;
}

echo "Las veces necesarias de elevar $number para que pase de 10.000 es: $count";