<?php

$colores = ['blanco', 'verde', 'rojo'];

foreach ($colores as $color) {
    echo $color.", ";
}

echo "<br>";

echo implode(", ", $colores);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array</title>
</head>
<body>
    <ul>
        <?php
            sort($colores);
            foreach ($colores as $color) {
                echo "<li> $color </li>";
            }
        ?>
    </ul>
</body>
</html>