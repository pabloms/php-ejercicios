<?php

require '../vendor/autoload.php';

$resolution = 3;
$useLegoPallete = false;
$legofy = new \RicardoFiorani\Legofy\Legofy();

$source = './perro.jpg';

$output = $legofy->convertToLego($source, $resolution, $useLegoPallete);

echo $output->response();