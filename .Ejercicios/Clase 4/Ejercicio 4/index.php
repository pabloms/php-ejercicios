<?php

    require_once 'Imagen.php';

    if(isset($_POST['title'])) {
        $title = $_POST['title'];
        $image = $_FILES['image'];

        $data = base64_encode(file_get_contents($image['tmp_name']));
        $type = $image['type'];

        $newImage = new Imagen($title, $type, $data); 
        $newImage->createFile();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles.css">
    <title>Fotos</title>
</head>
<body>
    <form method="POST" enctype="multipart/form-data">
        <h1>Imagen</h1>
        <input type="file" name="image">
        <h1>Nombre</h1>
        <input type="text" name="title">
        </br>
        </br>
        <input type="submit" value="Subir">
    </form>
    <hr>
    <form method="GET">
        <h1>Buscar Imagen</h1>
        <input type="text" name="search">
        <input type="submit" value="Buscar">
        <hr>
    </form>
    <div class="gallery">
        <?php
            if(isset($_GET['search'])) {
                $search = $_GET['search'];
                if (file_exists("images/" . $search)) {
                    $image = file_get_contents("images/" . $search);
                    
                    // function para obtener el tipo
                    $imgdata = base64_decode($image);
                    $f = finfo_open();
                    $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

                    echo "<img class='gallery-img' src='data:$mime_type;base64,$image'/>";
                } else {
                    echo "No se ha encontrado la imagen";
                }
            }
        ?> 
    </div>
</body>
</html>