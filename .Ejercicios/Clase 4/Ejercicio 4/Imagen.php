<?php

class Imagen {
    private $title;
    private $type;
    private $image;

    public function __construct($title, $type, $image)
    {
        $this->title = $title;
        $this->type = $type;
        $this->image = $image;
    }

    public function createFile() {
        $fileName = (new DateTime("now", new DateTimeZone("Europe/Madrid")))->format('dmyHis') . '.pablo';
        $fileRoute = "images/" . $fileName;

        file_put_contents($fileRoute, $this->image);

        echo "Imagen $this->title guardado, con el nombre: $fileName";
    }

}