<?php

class Book {
    private $title;
    private $author;
    private $genres;

    public function __construct($title, $author, array $genres)
    {
        $this->title = $title;
        $this->author = $author;
        $this->genres = $genres;
    }

    public function createTxt() {
        $titleTxt = str_replace(" ", "-", $this->title);
        $fileName = "./books/" . $titleTxt . ".txt";

        $fd = fopen(strtolower($fileName), 'a+');
        fclose($fd);
    }

    public function getBook() {
        return [
            "title"=>$this->title,
            "author"=>$this->author,
            "genres"=>$this->genres
        ];
    }
}