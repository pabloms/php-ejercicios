<?php

require_once 'Book.php';

if(isset($_POST['title']))
{
    $title = $_POST['title'];
    $author = $_POST['author'];
    $genres = $_POST['genres'];
    $genresArray = explode(",", $genres);

    $book = new Book($title, $author, $genresArray);
    $book->createTxt();

    $json = json_decode(file_get_contents('books.json'), true);

    array_push($json, $book->getBook());

    file_put_contents('books.json', json_encode($json));
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="POST">
        <h2>Titulo</h2>
        <input type="text" name="title">
        <h2>Autor</h2>
        <input type="text" name="author">
        <h2>Generos</h2>
        <input type="text" name="genres">
        <h2>Enviar</h2>
        <button type="submit">Click</button>
    </form>

    <h1>Mis Peliculas:</h1>

    <div>
        <?php
            $elements = json_decode(file_get_contents('books.json'), true);

            echo "<div>";
            foreach($elements as $element) {
                $title = $element["title"];
                $author = $element['author'];

                echo "<h1> $title </h1>
                    <h2> $author </h2>";

                echo "<ul>";
                foreach($element['genres'] as $genre) {
                    echo "<li> $genre </li>";
                }
                echo "</ul>";
                echo "<br>";
            }
            echo "</div>";
        ?>
    </div>
</body>
</html>