<?php

require_once 'Multimedia.php';

class Season extends Multimedia {

    private $number;
    private $year;
    private $episodes;

    public function __construct($title, $number, $year, array $episodes)
    {
        $this->title = $title;
        $this->number = $number;
        $this->year = $year;
        $this->episodes = $episodes;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }

    public function getEpisodes()
    {
        return $this->episodes;
    }

    public function setEpisodes($episodes)
    {
        $this->episodes = $episodes;
    }

    public function getEpisodeCount() {
        return count($this->episodes);
    }

    public function getRating()
    {
        $rating = 0;
        foreach($this->episodes as $episode) {
            $rating = $rating + $episode->getRating();
        }

        return $rating/count($this->episodes);
    }
}