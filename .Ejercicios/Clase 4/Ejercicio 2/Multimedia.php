<?php

abstract class Multimedia 
{
    // protegido es como privado pero lo pueden ver y usar su hijos
    protected $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
}