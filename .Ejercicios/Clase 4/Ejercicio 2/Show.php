<?php

require_once 'Multimedia.php';

class Show extends Multimedia {

    private $genre;
    private $seasons;

    public function __construct($title, $genre, array $seasons)
    {
        $this->title = $title;
        $this->genre = $genre;
        $this->seasons = $seasons;
    }

    public function geGenre()
    {
        return $this->genre;
    }

    public function setGenre($genre)
    {
        $this->genre = $genre;
    }

    public function getSeasons()
    {
        return $this->seasons;
    }

    public function setSeasons($seasons)
    {
        $this->seasons = $seasons;
    }

    public function getEpisodesCount() {
        $count = 0;

        foreach($this->seasons as $season) {
            $count = $count + $season->getEpisodeCount();
        }

        return $count;  
    }

    public function getRating() {
        $rating = 0;

        foreach($this->seasons as $season) {
            $rating = $rating + $season->getRating();
        }

        return $rating/count($this->seasons);
    }
}