<?php

require_once 'Multimedia.php';

class Episode extends Multimedia {

    private $number;
    private $date;
    private $rating;

    public function __construct($title, $date, $rating, $number)
    {
        $this->title = $title;
        $this->number = $number;
        $this->date = $date;
        $this->rating = $rating;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getRating()
    {
        return $this->rating;
    }

    public function setRating($rating)
    {
        $this->rating = $rating;
    }
}