<?php

require_once('Episode.php');
require_once('Season.php');
require_once('Show.php');


$ep1 = new Episode("Sally", new DateTime('2008-11-19'), 8.2, 1);
$ep2 = new Episode("Bret Gives Up the Dream", new DateTime('2007-06-24'), 8.3, 2);
$ep13 = new Episode("A Good Opportunity", new DateTime('2009-01-18'), 8.1, 1);
$ep14 = new Episode("New Cup", new DateTime('2009-01-25'), 8.7, 2);

$s1 = new Season('Season 1', 1, 2007, [$ep1, $ep2]);
$s2 = new Season('Season 2', 2, 2009, [$ep13, $ep14]);

$show = new Show('Flight of the Conchords', "Comedy", [$s1, $s2]);

echo $show->getTitle() . " tiene " . $show->getEpisodesCount() . " episodios.<br/>";
echo $show->getTitle() . " tiene una valoración media de " . $show->getRating() . ".";
