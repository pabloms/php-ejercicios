<?php

require_once "Persona.php";

$pablo = new Persona('Pablo', 75, 1.75);

echo $pablo->calcularIMC();

echo "<br>";

$pablo->estadoFisico();

echo "</br> --------- </br>";

$pepe = new Persona('Pepe', 100, 1.60);

echo $pepe->calcularIMC();

echo "<br>";

$pepe->estadoFisico();