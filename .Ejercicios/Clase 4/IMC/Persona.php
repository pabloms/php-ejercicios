<?php

class Persona {
    var $altura;
    var $nombre;
    var $peso;
    var $imc;

    function __construct(string $nombre, float $peso, float $altura)
    {
        $this->altura = $altura;
        $this->nombre = $nombre;
        $this->peso = $peso;
        $this->imc = 0;
    }
    
    // me da error si no le paso argumentos
    public function calcularIMC(): float 
    {
        $divisor = pow($this->altura, 2);
        $this->imc = $this->peso/$divisor;

        return $this->imc;
    }

    public function estadoFisico()
    {
        if($this->imc < 18.5) {
            echo "Delgado";
        } elseif ($this->imc >= 18.5 && $this->imc < 25) {
            echo "En la media";
        } elseif ($this->imc >= 25 && $this->imc < 30) {
            echo "Sobrepeso";
        } elseif ($this->imc >= 30) {
            echo "Obesidad";
        }
    }
    
}