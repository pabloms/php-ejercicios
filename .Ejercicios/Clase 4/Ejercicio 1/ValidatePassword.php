<?php

class Password {

    public function isValid($password) {
        if(strlen($password) < 8) {
            echo "La seguridad de la contraseña es baja";
        } elseif(strlen($password) > 8 && ctype_alnum($password)) {
            echo "La seguridad de la contraseña es media";
        } elseif(strlen($password) > 8 && !ctype_alnum($password)) {
            echo "La seguridad de la contraseña es alta";
        }
    }
}