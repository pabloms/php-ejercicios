<?php

namespace Upgrade\Poligono;

abstract class Poligono {

    protected $lado;
    protected $radio;

    abstract public function calcularArea();
}