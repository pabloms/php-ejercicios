<?php

namespace Upgrade\Cuadrado;
use Upgrade\Poligono\Poligono;

class Cuadrado extends Poligono {

    public function __construct($lado)
    {
        $this->lado = $lado;
    }

    public function calcularArea()
    {
        return pow($this->lado, 2);
    }
}