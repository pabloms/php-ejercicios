<?php

namespace Upgrade\Triangulo;
use Upgrade\Poligono\Poligono as Poligono;

class Triangulo extends Poligono {

    protected $lado2;

    public function __construct($base, $altura)
    {
        $this->lado = $base;
        $this->lado2 = $altura;
    }

    public function calcularArea()
    {
        return ($this->lado * $this->lado2) / 2;
    }
}