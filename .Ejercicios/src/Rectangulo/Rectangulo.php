<?php

namespace Upgrade\Rectangulo;
use Upgrade\Poligono\Poligono as Poligono;

class Rectangulo extends Poligono {

    protected $lado2;

    public function __construct($lado, $lado2)
    {
        $this->lado = $lado;
        $this->lado2 = $lado2;
    }

    public function calcularArea()
    {
        return $this->lado * $this->lado2;
    }
}