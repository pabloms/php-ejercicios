<?php

namespace Upgrade\Circulo;
use Upgrade\Poligono\Poligono as Poligono;

class Circulo extends Poligono {

    public function __construct($radio)
    {
        $this->radio = $radio;
    }

    public function calcularArea()
    {
        return pi() * pow($this->radio, 2);
    }
}