<?php

require '../vendor/autoload.php';

use Upgrade\Circulo\Circulo;
use Upgrade\Rectangulo\Rectangulo;
use Upgrade\Cuadrado\Cuadrado;
use Upgrade\Triangulo\Triangulo; // coge el ultimo "Triangulo"

$cuadrado = new Cuadrado(5);
$rectangulo = new Rectangulo(5, 10);
$circulo = new Circulo(5);
$triangulo = new Triangulo(10, 20);

echo "Circulo <br>";
echo $circulo->calcularArea();
echo "<br> Cuadrado <br>";
echo $cuadrado->calcularArea();
echo "<br> Rectangulo <br>";
echo $rectangulo->calcularArea();
echo "<br> Triangulo <br>";
echo $triangulo->calcularArea();

//packagist.com