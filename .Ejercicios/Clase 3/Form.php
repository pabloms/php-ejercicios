<?php
    if (isset($_FILES['images']) && isset($_POST['name'])) {
        $myImage = $_FILES['images'];
        $myName = $_POST['name'];

        $fileName = "./images/" . $myImage['name'];
        $fileName = str_replace(" ", "-", $fileName);
        copy($myImage['tmp_name'], $fileName);

        $fileName = $fileName . " " . $myName;

        file_put_contents("routes.txt", "\n" . $fileName, FILE_APPEND);
    } else {
        echo "Falta rellenar algun campo";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles.css">
    <title>Fotos</title>
</head>
<body>
    <form method="POST" enctype="multipart/form-data">
        <h1>Imagen</h1>
        <input type="file" name="images">
        <h1>Nombre</h1>
        <input type="text" name="name">
        </br>
        </br>
        <input type="submit" value="Subir">
    </form>
    <div class="gallery">
        <?php
            $file = file_get_contents("routes.txt");
            $routes = explode("\n", $file);
        
            foreach ($routes as $route) {
                $imprimir = explode(" ", $route);
                    echo "<div class='gallery-item'>
                            <h2>$imprimir[1]</h2>
                            <img class='gallery-img' src=$imprimir[0] alt=$imprimir[1]/>
                        </div>";
            }
        ?>
    </div>
</body>
</html>